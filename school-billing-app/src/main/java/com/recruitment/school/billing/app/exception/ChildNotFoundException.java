package com.recruitment.school.billing.app.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
@Getter
public class ChildNotFoundException extends RuntimeException {

    private final Long childId;

    public ChildNotFoundException(Long childId) {
        super("Not found child with id: " + childId);
        this.childId = childId;
    }
}