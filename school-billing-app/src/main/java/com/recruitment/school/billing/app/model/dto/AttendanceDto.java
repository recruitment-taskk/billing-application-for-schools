package com.recruitment.school.billing.app.model.dto;

import com.recruitment.school.billing.app.model.Attendance;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AttendanceDto {

    private String entryDate;

    private String exitDate;

    public static AttendanceDto from(Attendance attendance) {
        return AttendanceDto.builder()
                .entryDate(attendance.getEntryDate().toString())
                .exitDate(attendance.getExitDate().toString())
                .build();
    }
}
