package com.recruitment.school.billing.app.web;

import com.recruitment.school.billing.app.exception.ParentNotFoundException;
import com.recruitment.school.billing.app.exception.SchoolNotFoundException;
import com.recruitment.school.billing.app.model.dto.ChildDto;
import com.recruitment.school.billing.app.model.request.ChildCreateRequest;
import com.recruitment.school.billing.app.service.ChildService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/child")
@RequiredArgsConstructor
@Slf4j
public class ChildResource {

    private final ChildService childService;

    @PostMapping("/create")
    private ResponseEntity<?> createChild(@Valid @RequestBody ChildCreateRequest request) {
        log.info("Try to create child with parent id: {} and school id: {}", request.getParentId(), request.getSchoolId());
        try {
            ChildDto child = childService.createChild(request);
            return ResponseEntity.ok(child);
        } catch (SchoolNotFoundException | ParentNotFoundException exception) {
            log.error("Error while creating child for parent id: {} and school id: {}. Error message: {}",
                    request.getParentId(), request.getSchoolId(), exception.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getMessage());
        }
    }

}
