package com.recruitment.school.billing.app.service.impl;

import com.recruitment.school.billing.app.model.School;
import com.recruitment.school.billing.app.model.dto.SchoolDto;
import com.recruitment.school.billing.app.model.request.SchoolCreateRequest;
import com.recruitment.school.billing.app.repository.SchoolRepository;
import com.recruitment.school.billing.app.service.SchoolService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class SchoolServiceImpl implements SchoolService {

    private final SchoolRepository schoolRepository;

    @Override
    public SchoolDto createSchool(SchoolCreateRequest request) {
        School school = schoolRepository.save(School.builder()
                .name(request.getName())
                .hourPrice(BigDecimal.valueOf(request.getHourPrice()))
                .build());

        return SchoolDto.from(school);
    }

}
