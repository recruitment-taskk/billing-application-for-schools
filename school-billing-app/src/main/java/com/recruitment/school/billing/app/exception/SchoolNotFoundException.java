package com.recruitment.school.billing.app.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
@Getter
public class SchoolNotFoundException extends RuntimeException {

    private final Long schoolId;

    public SchoolNotFoundException(Long schoolId) {
        super("Not found school with id: " + schoolId);
        this.schoolId = schoolId;
    }

}