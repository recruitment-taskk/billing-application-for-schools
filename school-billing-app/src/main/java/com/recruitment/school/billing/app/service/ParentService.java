package com.recruitment.school.billing.app.service;

import com.recruitment.school.billing.app.model.dto.ParentDto;
import com.recruitment.school.billing.app.model.request.ParentCreateRequest;

public interface ParentService {

    ParentDto createParent(ParentCreateRequest request);

}