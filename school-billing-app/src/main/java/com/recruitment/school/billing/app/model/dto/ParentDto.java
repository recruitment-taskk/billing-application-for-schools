package com.recruitment.school.billing.app.model.dto;

import com.recruitment.school.billing.app.model.Child;
import com.recruitment.school.billing.app.model.Parent;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ParentDto {

    private String firstName;

    private String lastName;

    public static ParentDto from(Parent parent) {
        return ParentDto.builder()
                .firstName(parent.getFirstName())
                .lastName(parent.getLastName())
                .build();
    }

    public static ParentDto fromChild(Child child){
        return ParentDto.builder()
                .firstName(child.getParent().getFirstName())
                .lastName(child.getParent().getLastName())
                .build();
    }
}