package com.recruitment.school.billing.app.service;

import com.recruitment.school.billing.app.model.dto.SchoolDto;
import com.recruitment.school.billing.app.model.request.SchoolCreateRequest;

public interface SchoolService {

    SchoolDto createSchool(SchoolCreateRequest request);

}