package com.recruitment.school.billing.app.service.impl;

import com.recruitment.school.billing.app.exception.ChildNotFoundException;
import com.recruitment.school.billing.app.exception.InvalidAttendanceDatesException;
import com.recruitment.school.billing.app.model.Attendance;
import com.recruitment.school.billing.app.model.Child;
import com.recruitment.school.billing.app.model.dto.AttendanceDto;
import com.recruitment.school.billing.app.model.request.AttendanceCreateRequest;
import com.recruitment.school.billing.app.repository.AttendanceRepository;
import com.recruitment.school.billing.app.repository.ChildRepository;
import com.recruitment.school.billing.app.service.AttendanceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class AttendanceServiceImpl implements AttendanceService {

    private final AttendanceRepository attendanceRepository;
    private final ChildRepository childRepository;

    @Override
    @Transactional
    public AttendanceDto createAttendance(AttendanceCreateRequest request) {
        Child child = childRepository.findById(request.getChildId()).orElseThrow(() -> new ChildNotFoundException(request.getChildId()));

        checkIfAttendanceExitDateIsBiggerThenEntryDate(request);

        Attendance attendance = attendanceRepository.save(Attendance.builder()
                .entryDate(request.getEntryDate())
                .exitDate(request.getExitDate())
                .child(child)
                .build());
        child.getAttendances().add(attendance);

        return AttendanceDto.from(attendance);
    }

    private void checkIfAttendanceExitDateIsBiggerThenEntryDate(AttendanceCreateRequest request) {
        if (request.getExitDate().isBefore(request.getEntryDate())) {
            throw new InvalidAttendanceDatesException("Exit date cannot be before entry date.");
        }
    }
}
