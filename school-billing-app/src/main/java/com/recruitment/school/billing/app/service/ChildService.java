package com.recruitment.school.billing.app.service;

import com.recruitment.school.billing.app.model.dto.ChildDto;
import com.recruitment.school.billing.app.model.request.ChildCreateRequest;

public interface ChildService {

    ChildDto createChild(ChildCreateRequest request);

}