package com.recruitment.school.billing.app.web;

import com.recruitment.school.billing.app.model.dto.SchoolDto;
import com.recruitment.school.billing.app.model.request.SchoolCreateRequest;
import com.recruitment.school.billing.app.service.SchoolService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/school")
@RequiredArgsConstructor
@Slf4j
public class SchoolResource {

    private final SchoolService schoolService;


    @PostMapping("/create")
    ResponseEntity<SchoolDto> createSchool(@Valid @RequestBody SchoolCreateRequest request) {
        log.info("Try to create school");
        SchoolDto schoolDto = schoolService.createSchool(request);
        return new ResponseEntity<>(schoolDto, HttpStatus.CREATED);
    }

}