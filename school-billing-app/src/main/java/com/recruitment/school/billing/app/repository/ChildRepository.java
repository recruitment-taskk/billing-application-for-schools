package com.recruitment.school.billing.app.repository;

import com.recruitment.school.billing.app.model.Child;
import com.recruitment.school.billing.app.model.Parent;
import com.recruitment.school.billing.app.model.School;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChildRepository extends JpaRepository<Child, Long> {

    List<Child> findBySchool(School school);

    List<Child> findBySchoolAndParent(School school, Parent parent);

}