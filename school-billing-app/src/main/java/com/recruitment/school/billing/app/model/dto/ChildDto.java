package com.recruitment.school.billing.app.model.dto;

import com.recruitment.school.billing.app.model.Child;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChildDto {

    private String firstName;

    private String lastName;

    private ParentDto parentDto;

    public static ChildDto from(Child child){
        return ChildDto.builder()
                .firstName(child.getFirstName())
                .lastName(child.getLastName())
                .parentDto(ParentDto.fromChild(child))
                .build();
    }
}
