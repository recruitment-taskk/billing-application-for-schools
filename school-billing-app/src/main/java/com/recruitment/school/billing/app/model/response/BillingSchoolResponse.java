package com.recruitment.school.billing.app.model.response;

import com.recruitment.school.billing.app.model.School;
import com.recruitment.school.billing.app.model.dto.ChildBillingDetailsDto;
import com.recruitment.school.billing.app.model.dto.SchoolDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BillingSchoolResponse {

    private SchoolDto schoolDto;

    private Double totalBillingByMonth;

    private List<ChildBillingDetailsDto> childBillingDetailsDtoList;

    public static BillingSchoolResponse from(School school, Double totalBillingByMonth, List<ChildBillingDetailsDto> childBillingDetailsDtoList) {
        return BillingSchoolResponse.builder()
                .schoolDto(SchoolDto.from(school))
                .totalBillingByMonth(totalBillingByMonth)
                .childBillingDetailsDtoList(childBillingDetailsDtoList)
                .build();
    }

}