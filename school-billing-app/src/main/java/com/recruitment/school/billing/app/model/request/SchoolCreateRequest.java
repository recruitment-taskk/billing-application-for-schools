package com.recruitment.school.billing.app.model.request;

import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SchoolCreateRequest {

    @NotEmpty(message = "School name cannot be empty")
    private String name;

    @NotNull(message ="Hour price school cannot be null")
    @Digits(message = "School hour price must be a numeric value. Min value is 0.01", integer = 5, fraction = 2)
    private Double hourPrice;

}