package com.recruitment.school.billing.app.model.response;

import com.recruitment.school.billing.app.model.Parent;
import com.recruitment.school.billing.app.model.School;
import com.recruitment.school.billing.app.model.dto.ChildBillingDetailsDto;
import com.recruitment.school.billing.app.model.dto.ParentDto;
import com.recruitment.school.billing.app.model.dto.SchoolDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BillingParentResponse {

    private SchoolDto schoolDto;

    private ParentDto parentDto;

    private Double totalBillingByMonth;

    private List<ChildBillingDetailsDto> childBillingDetailsDtoList;

    public static BillingParentResponse from(School school, Parent parent, Double totalBillingByMonth,
                                             List<ChildBillingDetailsDto> childBillingDetailsDtoList) {
        return BillingParentResponse.builder()
                .schoolDto(SchoolDto.from(school))
                .parentDto(ParentDto.from(parent))
                .totalBillingByMonth(totalBillingByMonth)
                .childBillingDetailsDtoList(childBillingDetailsDtoList)
                .build();
    }

}