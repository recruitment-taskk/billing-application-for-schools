package com.recruitment.school.billing.app.service;

import com.recruitment.school.billing.app.model.request.ParentBillingRequest;
import com.recruitment.school.billing.app.model.response.BillingParentResponse;
import com.recruitment.school.billing.app.model.response.BillingSchoolResponse;
import com.recruitment.school.billing.app.model.request.SchoolBillingRequest;

public interface BillingService {

    BillingSchoolResponse calculateSchoolBilling(SchoolBillingRequest request);

    BillingParentResponse calculateParentBilling(ParentBillingRequest request);

}