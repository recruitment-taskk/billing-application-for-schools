package com.recruitment.school.billing.app.web;

import com.recruitment.school.billing.app.model.dto.ParentDto;
import com.recruitment.school.billing.app.model.request.ParentCreateRequest;
import com.recruitment.school.billing.app.service.ParentService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/parent")
@RequiredArgsConstructor
@Slf4j
public class ParentResource {

    private final ParentService parentService;


    @PostMapping("/create")
    private ResponseEntity<ParentDto> createParent(@Valid @RequestBody ParentCreateRequest request) {
        log.info("Try to create parent");
        ParentDto parentDto = parentService.createParent(request);
        return new ResponseEntity<>(parentDto, HttpStatus.CREATED);
    }
}
