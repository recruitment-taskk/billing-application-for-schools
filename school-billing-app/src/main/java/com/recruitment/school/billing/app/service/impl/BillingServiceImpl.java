package com.recruitment.school.billing.app.service.impl;

import com.recruitment.school.billing.app.exception.ParentNotFoundException;
import com.recruitment.school.billing.app.exception.SchoolNotFoundException;
import com.recruitment.school.billing.app.model.Attendance;
import com.recruitment.school.billing.app.model.Child;
import com.recruitment.school.billing.app.model.Parent;
import com.recruitment.school.billing.app.model.School;
import com.recruitment.school.billing.app.model.dto.ChildBillingDetailsDto;
import com.recruitment.school.billing.app.model.request.ParentBillingRequest;
import com.recruitment.school.billing.app.model.request.SchoolBillingRequest;
import com.recruitment.school.billing.app.model.response.BillingParentResponse;
import com.recruitment.school.billing.app.model.response.BillingSchoolResponse;
import com.recruitment.school.billing.app.repository.AttendanceRepository;
import com.recruitment.school.billing.app.repository.ChildRepository;
import com.recruitment.school.billing.app.repository.ParentRepository;
import com.recruitment.school.billing.app.repository.SchoolRepository;
import com.recruitment.school.billing.app.service.BillingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class BillingServiceImpl implements BillingService {

    private final SchoolRepository schoolRepository;
    private final ChildRepository childRepository;
    private final AttendanceRepository attendanceRepository;
    private final ParentRepository parentRepository;

    @Override
    public BillingSchoolResponse calculateSchoolBilling(SchoolBillingRequest request) {
        School school = schoolRepository.findById(request.getSchoolId())
                .orElseThrow(() -> new SchoolNotFoundException(request.getSchoolId()));

        double totalBillingAmountForSchool = 0;

        List<ChildBillingDetailsDto> childBillingDetailsDtoList = new ArrayList<>();

        List<Child> children = childRepository.findBySchool(school);

        for (Child child : children) {
            double childBillingAmount = processChildBillingDetailsForSchool(child, request, school, childBillingDetailsDtoList);
            totalBillingAmountForSchool += childBillingAmount;
        }

        return BillingSchoolResponse.from(school, totalBillingAmountForSchool, childBillingDetailsDtoList);
    }

    @Override
    public BillingParentResponse calculateParentBilling(ParentBillingRequest request) {
        Parent parent = parentRepository.findById(request.getParentId())
                .orElseThrow(() -> new ParentNotFoundException(request.getParentId()));

        School school = schoolRepository.findById(request.getSchoolId())
                .orElseThrow(() -> new SchoolNotFoundException(request.getSchoolId()));

        double totalBillingAmountForParent = 0;

        List<ChildBillingDetailsDto> childBillingDetailsDtoList = new ArrayList<>();

        List<Child> childeren = childRepository.findBySchoolAndParent(school, parent);

        for (Child child : childeren) {
            double childBillingAmount = processChildBillingDetailsForParent(child, request, school, childBillingDetailsDtoList);
            totalBillingAmountForParent += childBillingAmount;
        }

        return BillingParentResponse.from(school, parent, totalBillingAmountForParent, childBillingDetailsDtoList);
    }

    /*
    This method will return all hours in school for child in month(with free and paid hours)
     */
    private Long calculateNumberOfHoursInSchool(List<Attendance> attendances) {
        return attendances.stream()
                .mapToLong(attendance -> attendance.getExitDate().getHour() - attendance.getEntryDate().getHour())
                .sum();
    }

    /*
    This method will calculate child billing amount from his attendances
     */
    private BigDecimal calculateChildBillingAmount(List<Attendance> attendances, BigDecimal hourPrice) {
        long sumOfPaidHours = attendances.stream().mapToLong(this::returnPaidHoursByAttendance).sum();
        return hourPrice.multiply(BigDecimal.valueOf(sumOfPaidHours));
    }

    /*
   This method counts the paid hours based on the attendance entry date and exit date.
   This method assumes that child who entered and exited between 7:00 and 12:00, will not be charged.
   The method returns the calculated paid hours.
    */
    private long returnPaidHoursByAttendance(Attendance attendance) {
        LocalDateTime entryDate = attendance.getEntryDate();
        LocalDateTime exitDate = attendance.getExitDate();

        int entryHoursDiff = 0;
        int exitHoursDiff = 0;

        if (entryDate.getHour() < 7) {
            entryHoursDiff = 7 - entryDate.getHour();
        } else if (entryDate.getHour() >= 12) {
            entryHoursDiff = entryDate.getHour() - 11;
        }

        if (exitDate.getHour() < 7) {
            exitHoursDiff = 7 - exitDate.getHour();
        } else if (exitDate.getHour() >= 12) {
            exitHoursDiff = exitDate.getHour() - 11;
        }

        return entryHoursDiff + exitHoursDiff;
    }

    private double processChildBillingDetailsForParent(Child child, ParentBillingRequest request, School school,
                                                       List<ChildBillingDetailsDto> childBillingDetailsDtoList) {

        List<Attendance> attendancesChild = attendanceRepository.findByChildAndMounthAndYear(child, request.getMonth(), request.getYear());

        BigDecimal childBillingAmount = calculateChildBillingAmount(attendancesChild, school.getHourPrice());
        Long numberOfHoursAtSchoolPerMonth = calculateNumberOfHoursInSchool(attendancesChild);

        ChildBillingDetailsDto childBillingDetailsDto = ChildBillingDetailsDto.from(child,
                childBillingAmount.doubleValue(), numberOfHoursAtSchoolPerMonth);

        childBillingDetailsDtoList.add(childBillingDetailsDto);

        return childBillingAmount.doubleValue();
    }

    private double processChildBillingDetailsForSchool(Child child, SchoolBillingRequest request, School school,
                                                       List<ChildBillingDetailsDto> childBillingDetailsDtoList) {

        List<Attendance> attendancesChild = attendanceRepository.findByChildAndMounthAndYear(child, request.getMonth(), request.getYear());

        BigDecimal childBillingAmount = calculateChildBillingAmount(attendancesChild, school.getHourPrice());
        Long numberOfHoursAtSchoolPerMonth = calculateNumberOfHoursInSchool(attendancesChild);

        ChildBillingDetailsDto childBillingDetailsDto = ChildBillingDetailsDto.from(child,
                childBillingAmount.doubleValue(), numberOfHoursAtSchoolPerMonth);

        childBillingDetailsDtoList.add(childBillingDetailsDto);

        return childBillingAmount.doubleValue();
    }

}