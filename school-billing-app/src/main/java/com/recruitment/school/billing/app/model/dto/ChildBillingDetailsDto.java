package com.recruitment.school.billing.app.model.dto;

import com.recruitment.school.billing.app.model.Child;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChildBillingDetailsDto {

    private String childFirstName;

    private String childLastName;

    private ParentDto parentDto;

    private Double billingAmount;

    private Long numberOfHoursAtSchoolPerMonth;

    public static ChildBillingDetailsDto from(Child child, Double billingAmount, Long numberOfHoursAtSchoolPerMonth) {
        return ChildBillingDetailsDto.builder()
                .childFirstName(child.getFirstName())
                .childLastName(child.getLastName())
                .parentDto(ParentDto.fromChild(child))
                .billingAmount(billingAmount)
                .numberOfHoursAtSchoolPerMonth(numberOfHoursAtSchoolPerMonth)
                .build();
    }

}