package com.recruitment.school.billing.app.service;

import com.recruitment.school.billing.app.model.dto.AttendanceDto;
import com.recruitment.school.billing.app.model.request.AttendanceCreateRequest;

public interface AttendanceService {

    AttendanceDto createAttendance(AttendanceCreateRequest request);

}