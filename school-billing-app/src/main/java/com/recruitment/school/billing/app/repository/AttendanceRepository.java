package com.recruitment.school.billing.app.repository;

import com.recruitment.school.billing.app.model.Attendance;
import com.recruitment.school.billing.app.model.Child;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttendanceRepository extends JpaRepository<Attendance, Long> {

    @Query("SELECT att FROM Attendance att " +
            "WHERE att.child = :child " +
            "AND MONTH(att.entryDate) = :month " +
            "AND YEAR(att.entryDate) = :year")
    List<Attendance> findByChildAndMounthAndYear(Child child, int month, int year);

}