package com.recruitment.school.billing.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchoolBillingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchoolBillingApplication.class, args);
	}

}
