package com.recruitment.school.billing.app.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
@Getter
public class ParentNotFoundException extends RuntimeException {

    private final Long parentId;

    public ParentNotFoundException(Long parentId) {
        super("Not found parent with id: " + parentId);
        this.parentId = parentId;
    }

}
