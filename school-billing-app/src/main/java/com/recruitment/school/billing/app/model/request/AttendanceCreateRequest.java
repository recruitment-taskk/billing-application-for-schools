package com.recruitment.school.billing.app.model.request;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AttendanceCreateRequest {

    @NotNull(message = "child Id cannot be null")
    private Long childId;

    @NotNull(message = "Entry date cannot be null")
    private LocalDateTime entryDate;

    @NotNull(message = "Exit date cannot be null")
    private LocalDateTime exitDate;

}