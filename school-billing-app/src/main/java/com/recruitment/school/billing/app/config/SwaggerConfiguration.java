package com.recruitment.school.billing.app.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfiguration {

    @Bean
    public OpenAPI apiDocumetation(){
        return new OpenAPI()
                .info(new Info()
                        .description("Recruitment school billing application")
                        .title("Recruitment school billing application")
                        .version("1.0"));
    }
}