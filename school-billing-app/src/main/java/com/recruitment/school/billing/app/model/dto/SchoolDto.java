package com.recruitment.school.billing.app.model.dto;

import com.recruitment.school.billing.app.model.School;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SchoolDto {

    private String name;

    public static SchoolDto from(School entity){
        return SchoolDto.builder()
                .name(entity.getName())
                .build();
    }
}
