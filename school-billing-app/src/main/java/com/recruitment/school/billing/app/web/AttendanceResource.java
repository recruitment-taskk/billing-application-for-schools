package com.recruitment.school.billing.app.web;

import com.recruitment.school.billing.app.exception.ChildNotFoundException;
import com.recruitment.school.billing.app.exception.InvalidAttendanceDatesException;
import com.recruitment.school.billing.app.model.dto.AttendanceDto;
import com.recruitment.school.billing.app.model.request.AttendanceCreateRequest;
import com.recruitment.school.billing.app.service.AttendanceService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/attendance")
@RequiredArgsConstructor
@Slf4j
public class AttendanceResource {

    private final AttendanceService attendanceService;

    @PostMapping("/create")
    private ResponseEntity<?> createAttendance(@Valid @RequestBody AttendanceCreateRequest request) {
        log.info("Try to add attendance for child id {}", request.getChildId());
        try {
            AttendanceDto attendance = attendanceService.createAttendance(request);
            return ResponseEntity.ok(attendance);
        } catch (ChildNotFoundException exception) {
            log.error("Child with id: {} not found. Error message: {} :", request.getChildId(), exception.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getMessage());
        } catch (InvalidAttendanceDatesException exception) {
            log.error("Exit date {} is after entry date {}. Error message: {} ", request.getExitDate(),
                    request.getEntryDate(), exception.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
        }
    }

}