package com.recruitment.school.billing.app.model.request;

import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SchoolBillingRequest {

    @NotNull(message = "School id cannot be null")
    @Digits(message = "School id must be a numeric value", integer = 10, fraction = 0)
    @Positive(message = "School id must be a greater than 0")
    private Long schoolId;

    @NotNull(message = "Month cannot be null")
    @Digits(message = "Month must be a numeric value", integer = 2, fraction = 0)
    @Min(value = 1, message = "Month must be greater than 1 or equal 1")
    @Max(value = 12, message = "Month must be less than 12 or equal to 12")
    private int month;

    @NotNull(message = "Year cannot be null")
    @Digits(message = "Year must be a numeric value", integer = 4, fraction = 0)
    @Min(value = 2022, message = "Year must be greater than 2022 or equal to 2022")
    private int year;

}