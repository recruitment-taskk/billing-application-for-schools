package com.recruitment.school.billing.app.service.impl;

import com.recruitment.school.billing.app.model.Parent;
import com.recruitment.school.billing.app.model.dto.ParentDto;
import com.recruitment.school.billing.app.model.request.ParentCreateRequest;
import com.recruitment.school.billing.app.repository.ParentRepository;
import com.recruitment.school.billing.app.service.ParentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ParentServiceImpl implements ParentService {

    private final ParentRepository parentRepository;

    @Override
    public ParentDto createParent(ParentCreateRequest request) {
        Parent parent = parentRepository.save(Parent.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .build());

        return ParentDto.from(parent);
    }

}