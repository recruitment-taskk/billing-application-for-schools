package com.recruitment.school.billing.app.exception;

public class InvalidAttendanceDatesException extends RuntimeException{

    public InvalidAttendanceDatesException(String message) {
        super(message);
    }

    public InvalidAttendanceDatesException(String message, Throwable cause) {
        super(message, cause);
    }
}
