package com.recruitment.school.billing.app.web;

import com.recruitment.school.billing.app.exception.ParentNotFoundException;
import com.recruitment.school.billing.app.exception.SchoolNotFoundException;
import com.recruitment.school.billing.app.model.request.ParentBillingRequest;
import com.recruitment.school.billing.app.model.request.SchoolBillingRequest;
import com.recruitment.school.billing.app.model.response.BillingParentResponse;
import com.recruitment.school.billing.app.model.response.BillingSchoolResponse;
import com.recruitment.school.billing.app.service.BillingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("api/billing")
public class BillingResource {

    private final BillingService billingService;


    @PostMapping("/school")
    private ResponseEntity<?> calculateSchoolBilling(@Valid @RequestBody SchoolBillingRequest request) {
        log.info("Try to get school billing for school id: {}", request.getSchoolId());
        try {
            BillingSchoolResponse billingSchoolResponse = billingService.calculateSchoolBilling(request);
            return ResponseEntity.ok(billingSchoolResponse);
        } catch (SchoolNotFoundException exception) {
            log.error("Error while calculating school billing for school id: {}. Error message: {}",
                    request.getSchoolId(), exception.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getMessage());
        }
    }

    @PostMapping("/parent")
    private ResponseEntity<?> calculateParentBilling(@Valid @RequestBody ParentBillingRequest request) {
        log.info("Try to get parent billing for parent id: {} and school id: {}", request.getParentId(), request.getSchoolId());
        try {
            BillingParentResponse billingParentResponse = billingService.calculateParentBilling(request);
            return ResponseEntity.ok(billingParentResponse);
        } catch (SchoolNotFoundException | ParentNotFoundException exception) {
            log.error("Error while calculating parent billing for parent id: {} and school id: {}. Error message: {}",
                    request.getParentId(), request.getSchoolId(), exception.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getMessage());
        }
    }

}