package com.recruitment.school.billing.app.service.impl;

import com.recruitment.school.billing.app.exception.ParentNotFoundException;
import com.recruitment.school.billing.app.exception.SchoolNotFoundException;
import com.recruitment.school.billing.app.model.Child;
import com.recruitment.school.billing.app.model.Parent;
import com.recruitment.school.billing.app.model.School;
import com.recruitment.school.billing.app.model.dto.ChildDto;
import com.recruitment.school.billing.app.model.request.ChildCreateRequest;
import com.recruitment.school.billing.app.repository.ChildRepository;
import com.recruitment.school.billing.app.repository.ParentRepository;
import com.recruitment.school.billing.app.repository.SchoolRepository;
import com.recruitment.school.billing.app.service.ChildService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class ChildServiceImpl implements ChildService {

    private final ParentRepository parentRepository;
    private final SchoolRepository schoolRepository;
    private final ChildRepository childRepository;

    @Override
    @Transactional
    public ChildDto createChild(ChildCreateRequest request) {
        Parent parent = parentRepository.findById(request.getParentId()).orElseThrow(() -> new ParentNotFoundException(request.getParentId()));
        School school = schoolRepository.findById(request.getSchoolId()).orElseThrow(() -> new SchoolNotFoundException(request.getSchoolId()));

        Child child = childRepository.save(Child.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .parent(parent)
                .school(school)
                .build());

        parent.getChildren().add(child);
        school.getChildren().add(child);

        return ChildDto.from(child);
    }
}
