package com.recruitment.school.billing.app.model.request;

import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChildCreateRequest {

    @NotEmpty(message = "First name cannot be empty")
    private String firstName;

    @NotEmpty(message = "Last name cannot be empty")
    private String lastName;

    @NotNull(message = "School id cannot be null")
    @Digits(message = "School id must be a numeric value", integer = 10, fraction = 0)
    private Long schoolId;

    @Digits(message = "Parent id must be a numeric value", integer = 10, fraction = 0)
    @NotNull(message = "Parent id cannot be null")
    private Long parentId;

}