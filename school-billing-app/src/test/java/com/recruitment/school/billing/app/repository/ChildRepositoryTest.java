package com.recruitment.school.billing.app.repository;

import com.recruitment.school.billing.app.model.Child;
import com.recruitment.school.billing.app.model.Parent;
import com.recruitment.school.billing.app.model.School;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class ChildRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ChildRepository childRepository;

    @Test
    @DirtiesContext
    public void should_return_children_from_school() {
        // given
        Long schoolId1 = 1L;
        Long schoolId2 = 2L;
        Parent parent = entityManager.merge(prepareParent(1L));
        School school1 = entityManager.merge(prepareSchool(schoolId1));
        School school2 = entityManager.merge(prepareSchool(schoolId2));
        //and
        Child child1 = prepareChild(school1,parent);
        entityManager.persist(child1);
        Child child2 = prepareChild(school1,parent);
        entityManager.persist(child2);
        Child child3 = prepareChild(school2, parent);
        entityManager.persist(child3);
        // when
        List<Child> childrenFromSchool = childRepository.findBySchool(school1);
        // then
        assertEquals(2, childrenFromSchool.size());
        assertEquals(List.of(child1, child2), childrenFromSchool);
    }

    @Test
    @DirtiesContext
    public void should_return_children_by_parent_and_school() {
        // given
        Long schoolId1 = 1L;
        Long schoolId2 = 2L;
        Parent parent = entityManager.merge(prepareParent(1L));
        Parent parent2 = entityManager.merge(prepareParent(2L));
        School school1 = entityManager.merge(prepareSchool(schoolId1));
        School school2 = entityManager.merge(prepareSchool(schoolId2));
        //and
        Child child1 = prepareChild(school1,parent);
        entityManager.persist(child1);
        Child child2 = prepareChild(school1,parent);
        entityManager.persist(child2);
        Child child3 = prepareChild(school2, parent);
        entityManager.persist(child3);
        Child child4 = prepareChild(school2, parent2);
        entityManager.persist(child4);
        // when
        List<Child> childrenFromSchoolAndParent = childRepository.findBySchoolAndParent(school2, parent);
        // then
        assertEquals(1, childrenFromSchoolAndParent.size());
        assertEquals(List.of(child3), childrenFromSchoolAndParent);
    }

    private School prepareSchool(Long id) {
        return School.builder()
                .id(id)
                .name("Test School")
                .hourPrice(BigDecimal.TEN)
                .build();
    }

    private Parent prepareParent(Long id) {
        return Parent.builder()
                .id(id)
                .firstName("Jan")
                .lastName("Janowski")
                .build();
    }

    private Child prepareChild(School school, Parent parent) {
        return Child.builder()
                .firstName("Tomasz")
                .lastName("Adamek")
                .school(school)
                .parent(parent)
                .build();
    }

}