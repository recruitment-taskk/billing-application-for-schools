package com.recruitment.school.billing.app.service.impl;

import com.recruitment.school.billing.app.model.Attendance;
import com.recruitment.school.billing.app.model.Child;
import com.recruitment.school.billing.app.model.Parent;
import com.recruitment.school.billing.app.model.School;
import com.recruitment.school.billing.app.model.dto.ChildBillingDetailsDto;
import com.recruitment.school.billing.app.model.request.ParentBillingRequest;
import com.recruitment.school.billing.app.model.request.SchoolBillingRequest;
import com.recruitment.school.billing.app.model.response.BillingParentResponse;
import com.recruitment.school.billing.app.model.response.BillingSchoolResponse;
import com.recruitment.school.billing.app.repository.AttendanceRepository;
import com.recruitment.school.billing.app.repository.ChildRepository;
import com.recruitment.school.billing.app.repository.ParentRepository;
import com.recruitment.school.billing.app.repository.SchoolRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class BillingServiceImplTest {

    @Mock
    private SchoolRepository schoolRepository;

    @Mock
    private ChildRepository childRepository;

    @Mock
    private AttendanceRepository attendanceRepository;

    @Mock
    private ParentRepository parentRepository;

    @InjectMocks
    private BillingServiceImpl billingService;

    private static School school;

    private static List<Child> children;

    private static Parent parent;

    private static List<Attendance>attendancesFirstChild;

    private static List<Attendance>attendancesSecondChild;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        school = prepareSchool();
        parent = prepareParent();
        children = prepareChildrenList(school, parent);
        attendancesFirstChild = prepareAttendanceForFirstChild(children.get(0));
        attendancesSecondChild = prepareAttendanceForSecondChild(children.get(1));

    }


    @Test
    void should_calculate_and_return_school_billing() {
        //given
        SchoolBillingRequest request = prepareSchoolBillingRequest(school.getId());
        //when
        when(schoolRepository.findById(anyLong())).thenReturn(Optional.of(school));
        when(childRepository.findBySchool(any())).thenReturn(children);
        when(attendanceRepository.findByChildAndMounthAndYear(eq(children.get(0)), anyInt(), anyInt())).thenReturn(attendancesFirstChild);
        when(attendanceRepository.findByChildAndMounthAndYear(eq(children.get(1)), anyInt(), anyInt())).thenReturn(attendancesSecondChild);
        //and
        BillingSchoolResponse billingSchoolResponse = billingService.calculateSchoolBilling(request);
        //then
        verify(schoolRepository, times(1)).findById(request.getSchoolId());
        verify(childRepository, times(1)).findBySchool(school);
        verify(attendanceRepository, times(1)).findByChildAndMounthAndYear(children.get(0), request.getMonth(), request.getYear());
        verify(attendanceRepository, times(1)).findByChildAndMounthAndYear(children.get(1), request.getMonth(), request.getYear());
        //and
        assertNotNull(billingSchoolResponse);
        assertEquals(school.getName(), billingSchoolResponse.getSchoolDto().getName());
        assertEquals(100,  billingSchoolResponse.getTotalBillingByMonth());
        //and
        List<ChildBillingDetailsDto> childBillingDetailsList = billingSchoolResponse.getChildBillingDetailsDtoList();
        assertEquals(children.get(0).getFirstName(), childBillingDetailsList.get(0).getChildFirstName());
        assertEquals(children.get(0).getLastName(), childBillingDetailsList.get(0).getChildLastName());
        assertEquals(children.get(0).getParent().getFirstName(), childBillingDetailsList.get(0).getParentDto().getFirstName());
        assertEquals(children.get(0).getParent().getLastName(), childBillingDetailsList.get(0).getParentDto().getLastName());
        assertEquals(50, childBillingDetailsList.get(0).getBillingAmount());
        assertEquals(15, childBillingDetailsList.get(0).getNumberOfHoursAtSchoolPerMonth());
        //and
        assertEquals(children.get(1).getFirstName(), childBillingDetailsList.get(1).getChildFirstName());
        assertEquals(children.get(1).getLastName(), childBillingDetailsList.get(1).getChildLastName());
        assertEquals(children.get(1).getParent().getFirstName(), childBillingDetailsList.get(1).getParentDto().getFirstName());
        assertEquals(children.get(1).getParent().getLastName(), childBillingDetailsList.get(1).getParentDto().getLastName());
        assertEquals(50, childBillingDetailsList.get(1).getBillingAmount());
        assertEquals(15, childBillingDetailsList.get(1).getNumberOfHoursAtSchoolPerMonth());
    }

    @Test
    void should_calculate_and_return_parent_billing() {
        //given
        ParentBillingRequest request = prepareParentBillingRequest(parent, school);
        //when
        when(schoolRepository.findById(anyLong())).thenReturn(Optional.of(school));
        when(parentRepository.findById(anyLong())).thenReturn(Optional.of(parent));
        when(childRepository.findBySchoolAndParent(any(), any())).thenReturn(children);
        when(attendanceRepository.findByChildAndMounthAndYear(eq(children.get(0)), anyInt(), anyInt())).thenReturn(attendancesFirstChild);
        when(attendanceRepository.findByChildAndMounthAndYear(eq(children.get(1)), anyInt(), anyInt())).thenReturn(attendancesSecondChild);
        //and
        BillingParentResponse billingParentResponse = billingService.calculateParentBilling(request);
        //then
        verify(schoolRepository, times(1)).findById(request.getSchoolId());
        verify(childRepository, times(1)).findBySchoolAndParent(school, parent);
        verify(attendanceRepository, times(1)).findByChildAndMounthAndYear(children.get(0), request.getMonth(), request.getYear());
        verify(attendanceRepository, times(1)).findByChildAndMounthAndYear(children.get(1), request.getMonth(), request.getYear());
        //
        assertNotNull(billingParentResponse);
        assertEquals(school.getName(), billingParentResponse.getSchoolDto().getName());
        assertEquals(100,  billingParentResponse.getTotalBillingByMonth());
        //and
        List<ChildBillingDetailsDto> childBillingDetailsList = billingParentResponse.getChildBillingDetailsDtoList();
        assertEquals(children.get(0).getFirstName(), childBillingDetailsList.get(0).getChildFirstName());
        assertEquals(children.get(0).getLastName(), childBillingDetailsList.get(0).getChildLastName());
        assertEquals(children.get(0).getParent().getFirstName(), childBillingDetailsList.get(0).getParentDto().getFirstName());
        assertEquals(children.get(0).getParent().getLastName(), childBillingDetailsList.get(0).getParentDto().getLastName());
        assertEquals(50, childBillingDetailsList.get(0).getBillingAmount());
        assertEquals(15, childBillingDetailsList.get(0).getNumberOfHoursAtSchoolPerMonth());
        //and
        assertEquals(children.get(1).getFirstName(), childBillingDetailsList.get(1).getChildFirstName());
        assertEquals(children.get(1).getLastName(), childBillingDetailsList.get(1).getChildLastName());
        assertEquals(children.get(1).getParent().getFirstName(), childBillingDetailsList.get(1).getParentDto().getFirstName());
        assertEquals(children.get(1).getParent().getLastName(), childBillingDetailsList.get(1).getParentDto().getLastName());
        assertEquals(50, childBillingDetailsList.get(1).getBillingAmount());
        assertEquals(15, childBillingDetailsList.get(1).getNumberOfHoursAtSchoolPerMonth());
    }

    private School prepareSchool() {
        return School.builder()
                .id(1L)
                .name("School")
                .hourPrice(BigDecimal.TEN)
                .build();
    }

    private SchoolBillingRequest prepareSchoolBillingRequest(Long schoolId) {
        return SchoolBillingRequest.builder()
                .schoolId(schoolId)
                .month(1)
                .year(2024)
                .build();
    }

    private ParentBillingRequest prepareParentBillingRequest(Parent parent, School school){
        return ParentBillingRequest.builder()
                .parentId(parent.getId())
                .month(1)
                .year(2024)
                .schoolId(school.getId())
                .build();
    }

    private List<Child> prepareChildrenList(School school, Parent parent) {
        Child child1 = Child.builder()
                .id(1L)
                .firstName("Zosia")
                .lastName("Nowak")
                .school(school)
                .parent(parent)
                .build();
        Child child2 = Child.builder()
                .id(2L)
                .firstName("Adam")
                .lastName("Nowak")
                .school(school)
                .parent(parent)
                .build();
        return List.of(child1, child2);
    }

    private Parent prepareParent() {
        return Parent.builder()
                .id(1L)
                .firstName("Andrzej")
                .lastName("Nowak")
                .build();
    }

    private List<Attendance> prepareAttendanceForFirstChild(Child child) {
        Attendance attendance1 = Attendance.builder()
                .entryDate(LocalDateTime.of(2024, 1,5,6, 59))
                .exitDate(LocalDateTime.of(2024, 1,5,13, 1))
                .child(child)
                .build();
        Attendance attendance2 = Attendance.builder()
                .entryDate(LocalDateTime.of(2024, 1,6,8, 59))
                .exitDate(LocalDateTime.of(2024, 1,6,12, 59))
                .child(child)
                .build();
        Attendance attendance3 = Attendance.builder()
                .entryDate(LocalDateTime.of(2024, 1,7,6, 59))
                .exitDate(LocalDateTime.of(2024, 1,7,10, 59))
                .child(child)
                .build();
        return List.of(attendance1, attendance2, attendance3);
    }

    private List<Attendance> prepareAttendanceForSecondChild(Child child) {
        Attendance attendance1 = Attendance.builder()
                .entryDate(LocalDateTime.of(2024, 1,5,6, 59))
                .exitDate(LocalDateTime.of(2024, 1,5,13, 1))
                .child(child)
                .build();
        Attendance attendance2 = Attendance.builder()
                .entryDate(LocalDateTime.of(2024, 1,6,8, 59))
                .exitDate(LocalDateTime.of(2024, 1,6,12, 59))
                .child(child)
                .build();
        Attendance attendance3 = Attendance.builder()
                .entryDate(LocalDateTime.of(2024, 1,7,6, 59))
                .exitDate(LocalDateTime.of(2024, 1,7,10, 59))
                .child(child)
                .build();
        return List.of(attendance1, attendance2, attendance3);
    }

}