package com.recruitment.school.billing.app;

import com.recruitment.school.billing.app.repository.AttendanceRepository;
import com.recruitment.school.billing.app.repository.ChildRepository;
import com.recruitment.school.billing.app.repository.ParentRepository;
import com.recruitment.school.billing.app.repository.SchoolRepository;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class ApplicationBaseIT {

    public String localPort;

    @LocalServerPort
    private int randomServerPort;

    @Autowired
    protected AttendanceRepository attendanceRepository;

    @Autowired
    protected ChildRepository childRepository;

    @Autowired
    protected ParentRepository parentRepository;

    @Autowired
    protected SchoolRepository schoolRepository;

    @Autowired
    protected MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        this.localPort = "http://localhost:" + randomServerPort;
    }

}