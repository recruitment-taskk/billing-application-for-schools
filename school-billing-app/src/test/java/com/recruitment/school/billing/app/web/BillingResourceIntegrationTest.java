package com.recruitment.school.billing.app.web;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.recruitment.school.billing.app.ApplicationBaseIT;
import com.recruitment.school.billing.app.model.Parent;
import com.recruitment.school.billing.app.model.School;
import com.recruitment.school.billing.app.model.request.ParentBillingRequest;
import com.recruitment.school.billing.app.model.request.SchoolBillingRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;

import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class BillingResourceIntegrationTest extends ApplicationBaseIT {


    @Test
    void should_calculate_school_billing_and_return_status_ok() throws Exception {
        //given
        School school = schoolRepository.findById(1L).get();
        SchoolBillingRequest request = SchoolBillingRequest.builder()
                .schoolId(school.getId())
                .month(1)
                .year(2024)
                .build();
        //when & then
        mockMvc.perform(post(localPort + "/api/billing/school")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertToString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.schoolDto.name").value(school.getName()))
                .andExpect(jsonPath("$.totalBillingByMonth").value(198.0))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[0].childFirstName").value("Arkadiusz"))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[0].childLastName").value("Pieczen"))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[0].parentDto.firstName").value("Magdalena"))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[0].parentDto.lastName").value("Pieczen"))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[0].billingAmount").value(0.0))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[0].numberOfHoursAtSchoolPerMonth").value(0))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[1].childFirstName").value("Maciej"))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[1].childLastName").value("Korek"))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[1].parentDto.firstName").value("Anna"))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[1].parentDto.lastName").value("Korek"))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[1].billingAmount").value(99.0))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[1].numberOfHoursAtSchoolPerMonth").value(16))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[2].childFirstName").value("Arkadiusz"))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[2].childLastName").value("Korek"))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[2].parentDto.firstName").value("Anna"))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[2].parentDto.lastName").value("Korek"))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[2].billingAmount").value(99.0))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[2].numberOfHoursAtSchoolPerMonth").value(16));
    }

    @Test
    void should_calculate_parent_billing_and_return_status_ok() throws Exception {
        //given
        School school = schoolRepository.findById(2L).get();
        Parent parent = parentRepository.findById(1L).get();
        ParentBillingRequest request = ParentBillingRequest.builder()
                .schoolId(school.getId())
                .parentId(parent.getId())
                .month(1)
                .year(2024)
                .build();
        //when & then
        mockMvc.perform(post(localPort + "/api/billing/parent")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertToString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.schoolDto.name").value(school.getName()))
                .andExpect(jsonPath("$.parentDto.firstName").value(parent.getFirstName()))
                .andExpect(jsonPath("$.parentDto.lastName").value(parent.getLastName()))
                .andExpect(jsonPath("$.totalBillingByMonth").value(304.0))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[0].childFirstName").value("Mateusz"))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[0].childLastName").value("Kowalski"))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[0].parentDto.firstName").value(parent.getFirstName()))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[0].parentDto.lastName").value(parent.getLastName()))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[0].billingAmount").value(0.0))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[0].numberOfHoursAtSchoolPerMonth").value(0))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[1].childFirstName").value("Junior"))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[1].childLastName").value("Kowalski"))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[1].parentDto.firstName").value(parent.getFirstName()))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[1].parentDto.lastName").value(parent.getLastName()))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[1].billingAmount").value(304.0))
                .andExpect(jsonPath("$.childBillingDetailsDtoList[1].numberOfHoursAtSchoolPerMonth").value(32));
    }

    @ParameterizedTest
    @MethodSource("provideInvalidSchoolBillingRequests")
    void should_return_bad_request_for_invalid_school_billing_request(
            long schoolId, int month, int year, String expectedErrorMessage) throws Exception {
        //given
        SchoolBillingRequest request = SchoolBillingRequest.builder()
                .schoolId(schoolId)
                .month(month)
                .year(year)
                .build();
        //when & then
        mockMvc.perform(post(localPort + "/api/billing/school")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertToString(request)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]").value(expectedErrorMessage));
    }

    @ParameterizedTest
    @MethodSource("provideInvalidParentBillingRequests")
    void should_return_bad_request_for_invalid_parent_billing_request(
            long schoolId, long parentId, int month, int year, String expectedErrorMessage) throws Exception {
        //given
        ParentBillingRequest request = ParentBillingRequest.builder()
                .schoolId(schoolId)
                .parentId(parentId)
                .month(month)
                .year(year)
                .build();
        //when & then
        mockMvc.perform(post(localPort + "/api/billing/parent")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertToString(request)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]").value(expectedErrorMessage));
    }

    @Test
    void should_return_not_found_when_school_doesnt_exist_for_calculate_school_billing() throws Exception {
        //given
        Long schoolId = 99L;
        SchoolBillingRequest request = SchoolBillingRequest.builder()
                .schoolId(schoolId)
                .month(1)
                .year(2024)
                .build();
        //when & then
        mockMvc.perform(post(localPort + "/api/billing/school")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertToString(request)))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Not found school with id: " + schoolId));
    }

    @Test
    void should_return_not_found_when_school_doesnt_exist_for_calculate_parent_billing() throws Exception {
        //given
        Long schoolId = 99L;
        Parent parent = parentRepository.findById(1L).get();
        ParentBillingRequest request = ParentBillingRequest.builder()
                .schoolId(schoolId)
                .parentId(parent.getId())
                .month(1)
                .year(2024)
                .build();
        //when & then
        mockMvc.perform(post(localPort + "/api/billing/parent")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertToString(request)))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Not found school with id: " + schoolId));
    }

    @Test
    @DirtiesContext
    void should_return_not_found_when_parent_doesnt_exist_for_calculate_parent_billing() throws Exception {
        //given
        School school = schoolRepository.findById(1L).get();
        Long parentId = 99L;
        ParentBillingRequest request = ParentBillingRequest.builder()
                .schoolId(school.getId())
                .parentId(parentId)
                .month(1)
                .year(2024)
                .build();
        //when & then
        mockMvc.perform(post(localPort + "/api/billing/parent")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertToString(request)))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Not found parent with id: " + parentId));
    }

    private static Stream<Arguments> provideInvalidSchoolBillingRequests() {
        return Stream.of(
                Arguments.of(-1L, 1, 2024, "School id must be a greater than 0"),
                Arguments.of(1L, 15, 2024, "Month must be less than 12 or equal to 12"),
                Arguments.of(1L, 0, 2024, "Month must be greater than 1 or equal 1"),
                Arguments.of(1L, 5, 2020, "Year must be greater than 2022 or equal to 2022")
        );
    }

    private static Stream<Arguments> provideInvalidParentBillingRequests() {
        return Stream.of(
                Arguments.of(-1L, 1L, 1, 2024, "School id must be a greater than 0"),
                Arguments.of(1L, -1L, 1, 2024, "Parent id must be a greater than 0"),
                Arguments.of(1L, 1L, 15, 2024, "Month must be less than 12 or equal to 12"),
                Arguments.of(1L, 1L, 0, 2024, "Month must be greater than 1 or equal 1"),
                Arguments.of(1L, 1L, 5, 2020, "Year must be greater than 2022 or equal to 2022")
        );
    }

    private static String convertToString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}