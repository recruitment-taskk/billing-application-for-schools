package com.recruitment.school.billing.app.repository;

import com.recruitment.school.billing.app.model.Attendance;
import com.recruitment.school.billing.app.model.Child;
import com.recruitment.school.billing.app.model.Parent;
import com.recruitment.school.billing.app.model.School;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class AttendanceRepositoryTest {

    @Autowired
    private AttendanceRepository attendanceRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    @DirtiesContext
    public void should_return_list_of_attendance_from_child_mounth_and_year() {
        // given
        Parent parent = prepareParent();
        parent = entityManager.merge(parent);
        School school = prepareSchool();
        school = entityManager.merge(school);
        //and
        Child child = prepareChild(parent, school);
        child = entityManager.merge(child);
        entityManager.persist(child);
        //and
        List<Attendance> attendanceList = prepareAttendanceForChild(child);
        entityManager.persist(attendanceList.get(0));
        entityManager.persist(attendanceList.get(1));
        entityManager.persist(attendanceList.get(2));
        //and
        int month = 1;
        int year = 2024;
        // when
        List<Attendance> response = attendanceRepository.findByChildAndMounthAndYear(child, month, year);
        // then
        assertEquals(3, response.size());
        assertEquals(attendanceList.get(0).getEntryDate(), response.get(0).getEntryDate());
        assertEquals(attendanceList.get(0).getExitDate(), response.get(0).getExitDate());
        assertEquals(attendanceList.get(1).getEntryDate(), response.get(1).getEntryDate());
        assertEquals(attendanceList.get(1).getExitDate(), response.get(1).getExitDate());
        assertEquals(attendanceList.get(2).getEntryDate(), response.get(2).getEntryDate());
        assertEquals(attendanceList.get(2).getExitDate(), response.get(2).getExitDate());
        assertEquals(child, response.get(0).getChild());
        assertEquals(child, response.get(1).getChild());
        assertEquals(child, response.get(2).getChild());
    }


    private List<Attendance> prepareAttendanceForChild(Child child) {
        Attendance attendance1 = Attendance.builder()
                .entryDate(LocalDateTime.of(2024, 1, 5, 6, 59))
                .exitDate(LocalDateTime.of(2024, 1, 5, 13, 1))
                .child(child)
                .build();
        Attendance attendance2 = Attendance.builder()
                .entryDate(LocalDateTime.of(2024, 1, 6, 8, 59))
                .exitDate(LocalDateTime.of(2024, 1, 6, 12, 59))
                .child(child)
                .build();
        Attendance attendance3 = Attendance.builder()
                .entryDate(LocalDateTime.of(2024, 1, 7, 6, 59))
                .exitDate(LocalDateTime.of(2024, 1, 7, 10, 59))
                .child(child)
                .build();
        return List.of(attendance1, attendance2, attendance3);
    }

    private Child prepareChild(Parent parent, School school) {
        return Child.builder()
                .id(1L)
                .parent(parent)
                .school(school)
                .firstName("Zosia")
                .lastName("Nowak")
                .build();
    }

    private Parent prepareParent() {
        return Parent.builder()
                .id(1L)
                .firstName("Andrzej")
                .lastName("Nowak")
                .build();
    }

    private School prepareSchool() {
        return School.builder()
                .id(1L)
                .name("School")
                .hourPrice(BigDecimal.TEN)
                .build();
    }
}